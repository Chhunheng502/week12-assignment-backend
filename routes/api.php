<?php

use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('user/register', [UserController::class, 'register']);
Route::post('user/login', [UserController::class, 'login']);

Route::post('user/public', [UserController::class, 'createPublicList']);
Route::get('user/public', [UserController::class, 'getPublicList']);
Route::put('user/public/{id}', [UserController::class, 'editPublicList']);
Route::delete('user/public/{id}', [UserController::class, 'deletePublicList']);

Route::post('user/private/{id}', [UserController::class, 'createPrivateList']);
Route::get('user/private/{id}', [UserController::class, 'getPrivateList']);
Route::put('user/private/{user_id}/{item_id}', [UserController::class, 'editPrivateList']);
Route::delete('user/private/{user_id}/{item_id}', [UserController::class, 'deletePrivateList']);


// configure protected route
