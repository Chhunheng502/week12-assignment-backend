<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\PublicList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function createPublicList(Request $request)
    {
        $content = PublicList::create([
            'content' => $request->content
        ]);

        return response()->json(['result' => $content]);
    }

    public function getPublicList()
    {
        return PublicList::all();
    }

    public function editPublicList(Request $request, $id)
    {
        $data = PublicList::find($id);

        $data->content = $request->content;

        $data->save();

        return response()->json(['newContent' => $data]);
    }

    public function deletePublicList($id)
    {
        $data = PublicList::find($id);

        $result = $data->delete();

        if($result)
        {
            return $id . ' has been deleted';
        }
        else
        {
            return $id . ' has failed to delete';
        }
    }

    public function createPrivateList(Request $request, $id)
    {
        $user = User::find($id)->getList()->create([
            'content' => $request->content
        ]);

        return response()->json(['result' => $user]);
    }

    public function getPrivateList($id)
    {
        return User::find($id)->getList;
    }

    public function editPrivateList(Request $request, $user_id, $item_id)
    {
        $data = User::find($user_id)->getList()->find($item_id);

        $data->content = $request->content;

        $data->save();

        return response()->json(['newContent' => $data]);
    }

    public function deletePrivateList($user_id, $item_id)
    {
        $data = User::find($user_id)->getList()->find($item_id);

        $result = $data->delete();

        if($result)
        {
            return 'deleted';
        }
        else
        {
            return 'failed to delete';
        }
    }

    public function register(Request $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password)
        ]);

        return response()->json(['user' => $user]);
    }

    public function login(Request $request)
    {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if(Auth::attempt($credentials))
        {
            return response()->json(['user_id' => Auth::id()]);
        }
        else
        {
            return 'access_denied';
        }
    }
}
